import { Component, OnInit } from '@angular/core';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user = {
    email: "",
    password: ""
  }

  constructor(private userService: UserService) { }

  ngOnInit(): void {
  }

  loginUser(data) {
    console.log(data)
    this.userService.userLogin(data).subscribe(response =>{
      console.log(response)
    },error=>{
      console.log(error);
    })
  }
}

import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http:HttpClient) { }

  userLogin(obj) {
    return this.http.post('http://162.243.171.81:4000/users/login ',obj)
    .pipe(map((res: Response) => {
      return res;
    }));
  }
  

 userRegistration(data){
    return this.http.post(' http://162.243.171.81:4000/users/register',data)
    .pipe(map((res: Response) => {
      return res;
    }));
  }
}

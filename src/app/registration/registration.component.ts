import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {


  User ={
      firstName:"",
      lastName:"",
      email:"",
      password:"",
      mobileNumber:""
  }
  constructor(private userService: UserService,private router:Router) { }

  ngOnInit(): void {
  }

  addUser(obj){
    obj.role="User";
    console.log(obj)
     this.userService.userRegistration(obj).subscribe(response=>{
      console.log(response)
      this.router.navigate(["/login"]);
    },error=>{
      console.log(error)
    })
  }

}
